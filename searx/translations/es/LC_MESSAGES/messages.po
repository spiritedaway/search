# Translations template for PROJECT.
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
#
# Translators:
# Adam Tauber <asciimoo@gmail.com>, 2015
# Alejandro León Aznar, 2014
# Alejandro León Aznar, 2014-2018
# Carmen Fernández B., 2016
# novales35 <guillermocebollero@gmail.com>, 2020
# Juan Jaramillo <juanda097@protonmail.ch>, 2016
# Juan Jaramillo <juanda097@protonmail.ch>, 2017
# Marc Abonce Seguin, 2016
# Marc Abonce Seguin, 2018,2020
# O <b204fbaf817497f9ea35edbcc051de81_265921>, 2015
# rivera valdez <riveravaldezmail@gmail.com>, 2016
# wefwefew ewfewfewf <nnnedmz0d@moakt.ws>, 2016
# Agnes de Lion <irubiogafsi@pm.me>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: searx\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2021-06-18 02:49+0200\n"
"PO-Revision-Date: 2021-06-24 15:38+0000\n"
"Last-Translator: Agnes de Lion <irubiogafsi@pm.me>\n"
"Language-Team: Spanish <https://translate.tosdr.org/projects/tosdr-search/"
"main/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.5.1\n"
"Generated-By: Babel 2.7.0\n"

#: ../searx/webapp.py:164
msgid "files"
msgstr "Archivos"

#: ../searx/webapp.py:165
msgid "general"
msgstr "General"

#: ../searx/webapp.py:166
msgid "music"
msgstr "Música"

#: ../searx/webapp.py:167
msgid "social media"
msgstr "Medios sociales"

#: ../searx/webapp.py:168
msgid "images"
msgstr "Imágenes"

#: ../searx/webapp.py:169
msgid "videos"
msgstr "Vídeos"

#: ../searx/webapp.py:170
msgid "it"
msgstr "Informática"

#: ../searx/webapp.py:171
msgid "news"
msgstr "Noticias"

#: ../searx/webapp.py:172
msgid "map"
msgstr "Mapa"

#: ../searx/webapp.py:173
msgid "onions"
msgstr "onions"

#: ../searx/webapp.py:174
msgid "science"
msgstr "Ciencia"

#: ../searx/webapp.py:375
msgid "could not load data"
msgstr "No se pudo cargar los datos"

#: ../searx/webapp.py:377
msgid "No item found"
msgstr "Ningún artículo encontrado"

#: ../searx/webapp.py:483 ../searx/webapp.py:848
msgid "Invalid settings, please edit your preferences"
msgstr "Parámetros incorrectos, por favor, cambia tus preferencias"

#: ../searx/webapp.py:499
msgid "Invalid settings"
msgstr "Ajustes no válidos"

#: ../searx/webapp.py:564 ../searx/webapp.py:629
msgid "search error"
msgstr "error en la búsqueda"

#: ../searx/webapp.py:672
msgid "{minutes} minute(s) ago"
msgstr "hace {minutes} minuto(s)"

#: ../searx/webapp.py:674
msgid "{hours} hour(s), {minutes} minute(s) ago"
msgstr "hace {hours} hora(s) y {minutes} minuto(s)"

#: ../searx/answerers/statistics/answerer.py:54
msgid "Statistics functions"
msgstr "Funciones de estadística"

#: ../searx/answerers/statistics/answerer.py:55
msgid "Compute {functions} of the arguments"
msgstr "Computar {functions} de parámetros"

#: ../searx/engines/__init__.py:251
msgid "Engine time (sec)"
msgstr "Motor de tiempo (seg)"

#: ../searx/engines/__init__.py:255
msgid "Page loads (sec)"
msgstr "Tiempo de carga (segundos)"

#: ../searx/engines/__init__.py:259 ../searx/templates/oscar/results.html:34
msgid "Number of results"
msgstr "Número de resultados"

#: ../searx/engines/__init__.py:263
msgid "Scores"
msgstr "Puntuaciones"

#: ../searx/engines/__init__.py:267
msgid "Scores per result"
msgstr "Puntuaciones por resultado"

#: ../searx/engines/__init__.py:271
msgid "Errors"
msgstr "Errores"

#: ../searx/engines/openstreetmap.py:49
msgid "Get directions"
msgstr "Obtener indicaciones"

#: ../searx/engines/pdbe.py:90
msgid "{title} (OBSOLETE)"
msgstr "{title} (OBSOLETO)"

#: ../searx/engines/pdbe.py:97
msgid "This entry has been superseded by"
msgstr "Esta entrada la ha sustituido"

#: ../searx/engines/pubmed.py:78
msgid "No abstract is available for this publication."
msgstr "No hay resúmenes disponibles para esta publicación."

#: ../searx/plugins/better_answerer.py:9
msgid "Better Answerers"
msgstr "Mejores Respondedores"

#: ../searx/plugins/better_answerer.py:10
msgid "Module to improve the Answerer functionality in searx"
msgstr "Módulo para mejorar la funcionalidad de los Respondedores en Searx"

#: ../searx/plugins/hostname_replace.py:7
msgid "Hostname replace"
msgstr "Remplazar el nombre de equipo"

#: ../searx/plugins/hostname_replace.py:8
msgid "Rewrite result hostnames"
msgstr "Reescribir los nombres de equipo del resultado"

#: ../searx/plugins/https_rewrite.py:29
msgid "Rewrite HTTP links to HTTPS if possible"
msgstr "Cambiar los enlaces HTTP a HTTPS si es posible"

#: ../searx/plugins/infinite_scroll.py:3
msgid "Infinite scroll"
msgstr "Deslizamiento infinito"

#: ../searx/plugins/infinite_scroll.py:4
msgid "Automatically load next page when scrolling to bottom of current page"
msgstr ""
"Cargar automáticamente la siguiente página al deslizarse hasta el final de "
"la página actual"

#: ../searx/plugins/oa_doi_rewrite.py:9
msgid "Open Access DOI rewrite"
msgstr "Reescribir acceso abierto a DOI (Identificador de objeto digital)"

#: ../searx/plugins/oa_doi_rewrite.py:10
msgid ""
"Avoid paywalls by redirecting to open-access versions of publications when "
"available"
msgstr ""
"Evitar barreras de pago redireccionando a las versiones de acceso libre de "
"las publicaciones cuando estén disponibles"

#: ../searx/plugins/search_on_category_select.py:18
msgid "Search on category select"
msgstr "Buscar en la categoría seleccionada"

#: ../searx/plugins/search_on_category_select.py:19
msgid ""
"Perform search immediately if a category selected. Disable to select "
"multiple categories. (JavaScript required)"
msgstr ""
"Realizar una búsqueda inmediatamente si se ha seleccionado una categoría. "
"Desactivar para seleccionar varias categorías. (Se requiere JavaScript)"

#: ../searx/plugins/self_info.py:19
#, fuzzy
#| msgid "Information!"
msgid "Self Informations"
msgstr "¡Información!"

#: ../searx/plugins/self_info.py:20
msgid ""
"Displays your IP if the query is \"ip\" and your user agent if the query "
"contains \"user agent\"."
msgstr ""
"Muestra tu dirección IP si la consulta es \"ip\" y tu Agente de Usuario si "
"la consulta contiene \"agente de usuario\"."

#: ../searx/plugins/tracker_url_remover.py:27
msgid "Tracker URL remover"
msgstr "Eliminador de URL rastreadora"

#: ../searx/plugins/tracker_url_remover.py:28
msgid "Remove trackers arguments from the returned URL"
msgstr "Eliminar los argumentos de los rastreadores en la URL devuelta"

#: ../searx/plugins/vim_hotkeys.py:3
msgid "Vim-like hotkeys"
msgstr "Teclas de acceso rápido como-Vim"

#: ../searx/plugins/vim_hotkeys.py:4
msgid ""
"Navigate search results with Vim-like hotkeys (JavaScript required). Press "
"\"h\" key on main or result page to get help."
msgstr ""
"Navegar por los resultados de búsqueda con las teclas de acceso rápido como-"
"Vim (es necesario JavaScript). Pulse la tecla \"h\"  en la página principal "
"o en el resultado para obtener ayuda."

#: ../searx/plugins/answerer/alternative/answerer.py:24
msgid "The ToS;DR Team"
msgstr "El Equipo ToS;DR"

#: ../searx/plugins/answerer/alternative/answerer.py:85
msgid "Privacy Alternative"
msgstr ""

#: ../searx/plugins/answerer/alternative/answerer.py:86
msgid "Get privacy friendly alternatives from popular services."
msgstr ""

#: ../searx/plugins/answerer/alternative/answerer.py:90
msgid "Report a bug to the maintainer"
msgstr ""

#: ../searx/plugins/answerer/alternative/templates/answer.html:1
msgid "Privacy friendly alternatives to {service}"
msgstr ""

#: ../searx/plugins/answerer/ascii/answerer.py:41
#, fuzzy
#| msgid "Shortcut"
msgid "Ascii Shortcuts"
msgstr "Atajo"

#: ../searx/plugins/answerer/ascii/answerer.py:42
msgid "Does some Ascii Stuff"
msgstr ""

#: ../searx/plugins/answerer/bitcoin/answerer.py:96
msgid "Bitcoin Index"
msgstr ""

#: ../searx/plugins/answerer/bitcoin/answerer.py:97
msgid "Get a the bitcoin index via currency or by default via USD, EUR and GBP"
msgstr ""

#: ../searx/plugins/answerer/bitcoin/templates/all.html:1
#: ../searx/plugins/answerer/bitcoin/templates/currency.html:1
msgid "The current Bitcoin index is"
msgstr ""

#: ../searx/plugins/answerer/cheatsheets/answerer.py:79
msgid "Cheatsheet Index"
msgstr ""

#: ../searx/plugins/answerer/cheatsheets/answerer.py:80
msgid "Cheatsheet module to load some awesome modules found around the web."
msgstr ""

#: ../searx/plugins/answerer/cheatsheets/modules/guitar/templates/all.html:1
msgid "Guitar Cheatsheet"
msgstr ""

#: ../searx/plugins/answerer/cheatsheets/modules/guitar/templates/all.html:24
msgid "Cheatsheet Images by {source}"
msgstr ""

#: ../searx/plugins/answerer/grade/answerer.py:82
msgid "{service} has a Privacy {grade} on ToS;DR"
msgstr ""

#: ../searx/plugins/answerer/grade/answerer.py:116
msgid "ToS;DR Grade"
msgstr ""

#: ../searx/plugins/answerer/grade/answerer.py:117
msgid "Get a grade from ToS;DR using the ToS;DR API"
msgstr ""

#: ../searx/plugins/answerer/hash/answerer.py:69
msgid "Hash Generator"
msgstr ""

#: ../searx/plugins/answerer/hash/answerer.py:70
msgid "Generate md4, md5, sha1, sha256 and sha512 hashes"
msgstr ""

#: ../searx/plugins/answerer/leetspeak/answerer.py:64
msgid "Leetspeak Answerer"
msgstr ""

#: ../searx/plugins/answerer/leetspeak/answerer.py:65
msgid "L 3 3 '][' 5 |D 3 /-\\ |< I 5 C 0 0 |_"
msgstr ""

#: ../searx/plugins/answerer/nato/answerer.py:66
msgid "Nato Converter"
msgstr ""

#: ../searx/plugins/answerer/nato/answerer.py:67
msgid "Converts words into the nato phonetic alphabet"
msgstr ""

#: ../searx/plugins/answerer/onion/answerer.py:40
msgid "Onion Notice"
msgstr ""

#: ../searx/plugins/answerer/onion/answerer.py:41
msgid "Give a notice if an onion address has been queried"
msgstr ""

#: ../searx/plugins/answerer/random/answerer.py:94
msgid "Random value generator"
msgstr "Generador de valores aleaorios"

#: ../searx/plugins/answerer/random/answerer.py:95
msgid "Generate different random values"
msgstr "Generar varios valores aleatorios"

#: ../searx/templates/oscar/404.html:4
msgid "Page not found"
msgstr "Página no encontrada"

#: ../searx/templates/oscar/404.html:6
#, python-format
msgid "Go to %(search_page)s."
msgstr "Ir a %(search_page)s."

#: ../searx/templates/oscar/404.html:6
msgid "search page"
msgstr "Página de búsqueda"

#: ../searx/templates/oscar/about.html:2 ../searx/templates/oscar/navbar.html:6
msgid "about"
msgstr "acerca de"

#: ../searx/templates/oscar/advanced.html:4
msgid "Advanced settings"
msgstr "Ajustes avanzados"

#: ../searx/templates/oscar/base.html:77
#: ../searx/templates/oscar/messages/first_time.html:4
#: ../searx/templates/oscar/messages/save_settings_successfull.html:5
#: ../searx/templates/oscar/messages/unknow_error.html:5
msgid "Close"
msgstr "Cerrar"

#: ../searx/templates/oscar/base.html:79
#: ../searx/templates/oscar/messages/no_results.html:4
msgid "Error!"
msgstr "¡Error!"

#: ../searx/templates/oscar/base.html:107
msgid "Powered by {searx} and forked by {tosdr}"
msgstr ""

#: ../searx/templates/oscar/base.html:108
msgid "{version} on commit {hash}"
msgstr ""

#: ../searx/templates/oscar/base.html:109
msgid "a privacy-respecting, hackable metasearch engine"
msgstr "un metabuscador hackable que respeta la privacidad"

#: ../searx/templates/oscar/base.html:110
msgid "Source code"
msgstr "Código fuente"

#: ../searx/templates/oscar/base.html:111
msgid "Issue tracker"
msgstr "Gestor de incidencias"

#: ../searx/templates/oscar/base.html:113
msgid "Contact instance maintainer"
msgstr ""

#: ../searx/templates/oscar/base.html:115
msgid "Translate ToS;DR Search"
msgstr ""

#: ../searx/templates/oscar/languages.html:2
msgid "Language"
msgstr "Lenguaje"

#: ../searx/templates/oscar/languages.html:4
msgid "Default language"
msgstr "Idioma por defecto"

#: ../searx/templates/oscar/macros.html:23
msgid "magnet link"
msgstr "magnet link (enlace sin archivo)"

#: ../searx/templates/oscar/macros.html:24
msgid "torrent file"
msgstr "archivo torrent"

#: ../searx/templates/oscar/macros.html:36
#: ../searx/templates/oscar/macros.html:38
#: ../searx/templates/oscar/macros.html:72
#: ../searx/templates/oscar/macros.html:74
msgid "cached"
msgstr "en caché"

#: ../searx/templates/oscar/macros.html:42
#: ../searx/templates/oscar/macros.html:58
#: ../searx/templates/oscar/macros.html:78
#: ../searx/templates/oscar/macros.html:92
msgid "proxied"
msgstr "proxied"

#: ../searx/templates/oscar/macros.html:132
#: ../searx/templates/oscar/preferences.html:252
#: ../searx/templates/oscar/preferences.html:269
msgid "Allow"
msgstr "Permitir"

#: ../searx/templates/oscar/macros.html:139
msgid "supported"
msgstr "soportado"

#: ../searx/templates/oscar/macros.html:143
msgid "not supported"
msgstr "no soportado"

#: ../searx/templates/oscar/navbar.html:7
#: ../searx/templates/oscar/preferences.html:16
msgid "preferences"
msgstr "preferencias"

#: ../searx/templates/oscar/preferences.html:12
msgid "No HTTPS"
msgstr ""

#: ../searx/templates/oscar/preferences.html:21
msgid "Preferences"
msgstr "Preferencias"

#: ../searx/templates/oscar/preferences.html:26
#: ../searx/templates/oscar/preferences.html:36
msgid "General"
msgstr "General"

#: ../searx/templates/oscar/preferences.html:27
#: ../searx/templates/oscar/preferences.html:226
msgid "Engines"
msgstr "Motores"

#: ../searx/templates/oscar/preferences.html:28
#: ../searx/templates/oscar/preferences.html:329
msgid "Plugins"
msgstr "Plugins"

#: ../searx/templates/oscar/preferences.html:30
#: ../searx/templates/oscar/preferences.html:359
msgid "Answerers"
msgstr "Respondedores"

#: ../searx/templates/oscar/preferences.html:31
#: ../searx/templates/oscar/preferences.html:412
msgid "Cookies"
msgstr "Cookies"

#: ../searx/templates/oscar/preferences.html:49
#: ../searx/templates/oscar/preferences.html:52
msgid "Default categories"
msgstr "Categorías predeterminadas"

#: ../searx/templates/oscar/preferences.html:60
msgid "Search language"
msgstr "Idioma de búsqueda"

#: ../searx/templates/oscar/preferences.html:61
msgid "What language do you prefer for search?"
msgstr "¿Qué idioma prefieres para la búsqueda?"

#: ../searx/templates/oscar/preferences.html:68
msgid "Interface language"
msgstr "Idioma de la interfaz"

#: ../searx/templates/oscar/preferences.html:69
msgid "Change the language of the layout"
msgstr "Cambiar idioma de la interfaz"

#: ../searx/templates/oscar/preferences.html:81
msgid "Autocomplete"
msgstr "Autocompletar"

#: ../searx/templates/oscar/preferences.html:82
msgid "Find stuff as you type"
msgstr "Buscar mientras escribes"

#: ../searx/templates/oscar/preferences.html:96
msgid "Image proxy"
msgstr "Imagen proxy"

#: ../searx/templates/oscar/preferences.html:97
msgid "Proxying image results through searx"
msgstr "Filtrado de resultados de imágenes en searx"

#: ../searx/templates/oscar/preferences.html:102
msgid "Enabled"
msgstr "Activado"

#: ../searx/templates/oscar/preferences.html:104
msgid "Disabled"
msgstr "Desactivado"

#: ../searx/templates/oscar/preferences.html:110
msgid "Method"
msgstr "Método"

#: ../searx/templates/oscar/preferences.html:111
msgid ""
"Change how forms are submited, <a href=\"http://en.wikipedia.org/wiki/"
"Hypertext_Transfer_Protocol#Request_methods\" rel=\"external\">learn more "
"about request methods</a>"
msgstr ""
"Modifica cómo se envian los formularios <a href=\"http://es.wikipedia.org/"
"wiki/Hypertext_Transfer_Protocol#M.C3.A9todos_de_petici.C3.B3n\" rel="
"\"external\">más información sobre métodos de peticiones</a>"

#: ../searx/templates/oscar/preferences.html:123
#: ../searx/templates/oscar/preferences.html:256
#: ../searx/templates/oscar/preferences.html:264
msgid "SafeSearch"
msgstr "Búsqueda segura"

#: ../searx/templates/oscar/preferences.html:124
msgid "Filter content"
msgstr "Filtro de contenido"

#: ../searx/templates/oscar/preferences.html:129
msgid "Strict"
msgstr "Riguroso"

#: ../searx/templates/oscar/preferences.html:131
msgid "Moderate"
msgstr "Moderado"

#: ../searx/templates/oscar/preferences.html:133
msgid "None"
msgstr "Ninguno"

#: ../searx/templates/oscar/preferences.html:139
msgid "Themes"
msgstr "Temas"

#: ../searx/templates/oscar/preferences.html:140
msgid "Change searx layout"
msgstr "Cambiar aspecto de searx"

#: ../searx/templates/oscar/preferences.html:152
#: ../searx/templates/oscar/preferences.html:160
msgid "Choose style for this theme"
msgstr "Elige un estilo para este tema"

#: ../searx/templates/oscar/preferences.html:152
#: ../searx/templates/oscar/preferences.html:160
msgid "Style"
msgstr "Estilo"

#: ../searx/templates/oscar/preferences.html:164
msgid "Results on new tabs"
msgstr "Resultados en nuevas pestañas"

#: ../searx/templates/oscar/preferences.html:165
msgid "Open result links on new browser tabs"
msgstr "Abrir los enlaces resultantes en nuevas pestañas del navegador"

#: ../searx/templates/oscar/preferences.html:170
#: ../searx/templates/oscar/preferences.html:183
msgid "On"
msgstr "Activado"

#: ../searx/templates/oscar/preferences.html:172
#: ../searx/templates/oscar/preferences.html:185
msgid "Off"
msgstr "Desactivado"

#: ../searx/templates/oscar/preferences.html:177
#, fuzzy
#| msgid "Advanced settings"
msgid "Show advanced settings"
msgstr "Ajustes avanzados"

#: ../searx/templates/oscar/preferences.html:178
msgid "Show advanced settings panel in the home page by default"
msgstr ""

#: ../searx/templates/oscar/preferences.html:190
msgid "Open Access DOI resolver"
msgstr ""

#: ../searx/templates/oscar/preferences.html:191
msgid ""
"Redirect to open-access versions of publications when available (plugin "
"required)"
msgstr ""
"Redireccionar a versiones de acceso abierto de las publicaciones cuando "
"estén disponibles (se requiere plugin)"

#: ../searx/templates/oscar/preferences.html:205
msgid "Engine tokens"
msgstr ""

#: ../searx/templates/oscar/preferences.html:206
msgid "Access tokens for private engines"
msgstr ""

#: ../searx/templates/oscar/preferences.html:235
msgid "Allow all"
msgstr "Permitir todo"

#: ../searx/templates/oscar/preferences.html:237
msgid "Disable all"
msgstr "Deshabilitar todo"

#: ../searx/templates/oscar/preferences.html:253
#: ../searx/templates/oscar/preferences.html:268
msgid "Engine name"
msgstr "Nombre del motor de búsqueda"

#: ../searx/templates/oscar/preferences.html:254
#: ../searx/templates/oscar/preferences.html:267
msgid "Shortcut"
msgstr "Atajo"

#: ../searx/templates/oscar/preferences.html:255
#: ../searx/templates/oscar/preferences.html:266
msgid "Selected language"
msgstr "Idioma elegido"

#: ../searx/templates/oscar/preferences.html:257
#: ../searx/templates/oscar/preferences.html:263
#: ../searx/templates/oscar/time-range.html:2
msgid "Time range"
msgstr "Rango de tiempo"

#: ../searx/templates/oscar/preferences.html:258
#: ../searx/templates/oscar/preferences.html:262
msgid "Avg. time"
msgstr "Tiempo promedio"

#: ../searx/templates/oscar/preferences.html:259
#: ../searx/templates/oscar/preferences.html:261
msgid "Max time"
msgstr "Tiempo máximo"

#: ../searx/templates/oscar/preferences.html:362
msgid "This is the list of searx's instant answering modules."
msgstr "Esta es la lista de los módulos de respuesta inmediata de searx."

#: ../searx/templates/oscar/preferences.html:366
msgid "Name"
msgstr "Nombre"

#: ../searx/templates/oscar/preferences.html:367
#: ../searx/templates/oscar/results.html:191
#: ../searx/templates/oscar/results.html:196
#: ../searx/templates/oscar/results.html:204
#: ../searx/templates/oscar/results.html:209
msgid "Developer"
msgstr ""

#: ../searx/templates/oscar/preferences.html:368
msgid "Description"
msgstr "Descripción"

#: ../searx/templates/oscar/preferences.html:369
msgid "Examples"
msgstr "Ejemplos"

#: ../searx/templates/oscar/preferences.html:415
msgid ""
"This is the list of cookies and their values searx is storing on your "
"computer."
msgstr ""
"Esta es la lista de cookies y sus valores que searx está almacenando en tu "
"ordenador."

#: ../searx/templates/oscar/preferences.html:416
msgid "With that list, you can assess searx transparency."
msgstr "Con esa lista puedes valorar la transparencia de searx."

#: ../searx/templates/oscar/preferences.html:421
msgid "Cookie name"
msgstr "Nombre de la cookie"

#: ../searx/templates/oscar/preferences.html:422
msgid "Value"
msgstr "Valor"

#: ../searx/templates/oscar/preferences.html:439
msgid ""
"These settings are stored in your cookies, this allows us not to store this "
"data about you."
msgstr ""
"Esta configuración se guarda en sus cookies, lo que nos permite no almacenar "
"dicha información sobre usted."

#: ../searx/templates/oscar/preferences.html:440
msgid ""
"These cookies serve your sole convenience, we don't use these cookies to "
"track you."
msgstr ""
"Estas cookies son para su propia comodidad, no las utilizamos para "
"rastrearle."

#: ../searx/templates/oscar/preferences.html:444
msgid "Search URL of the currently saved preferences"
msgstr "Buscar URL de las preferencias guardadas actualmente"

#: ../searx/templates/oscar/preferences.html:445
msgid ""
"Note: specifying custom settings in the search URL can reduce privacy by "
"leaking data to the clicked result sites."
msgstr ""
"Nota: especificar configuraciones personalizadas en la URL de búsqueda puede "
"reducir la privacidad al filtrar datos a los sitios de resultados en los que "
"se ha hecho clic."

#: ../searx/templates/oscar/preferences.html:452
msgid "save"
msgstr "Guardar"

#: ../searx/templates/oscar/preferences.html:454
msgid "back"
msgstr "Atrás"

#: ../searx/templates/oscar/preferences.html:457
msgid "Reset defaults"
msgstr "Restablecer configuración por defecto"

#: ../searx/templates/oscar/results.html:39
msgid "Engines cannot retrieve results"
msgstr "Los motores no pueden obtener resultados"

#: ../searx/templates/oscar/results.html:55
msgid "Suggestions"
msgstr "Sugerencias"

#: ../searx/templates/oscar/results.html:77
msgid "Links"
msgstr "Enlaces"

#: ../searx/templates/oscar/results.html:82
msgid "Search URL"
msgstr "URL de la búsqueda"

#: ../searx/templates/oscar/results.html:88
msgid "Download results"
msgstr "Descargar resultados"

#: ../searx/templates/oscar/results.html:100
msgid "RSS subscription"
msgstr "Suscripción RSS "

#: ../searx/templates/oscar/results.html:107
msgid "Search results"
msgstr "Resultados de búsqueda"

#: ../searx/templates/oscar/results.html:112
msgid "Try searching for:"
msgstr "Intenta buscar:"

#: ../searx/templates/oscar/results.html:159
msgid "No Description provided by the developer"
msgstr ""

#: ../searx/templates/oscar/results.html:166
#, fuzzy
#| msgid "Issue tracker"
msgid "Issue Tracker"
msgstr "Gestor de incidencias"

#: ../searx/templates/oscar/results.html:175
msgid ""
"Source\n"
"                                                        Code"
msgstr ""

#: ../searx/templates/oscar/results.html:183
msgid "Website"
msgstr ""

#: ../searx/templates/oscar/results.html:253
#: ../searx/templates/oscar/results.html:283
#: ../searx/templates/oscar/results.html:296
msgid "Missing Text"
msgstr ""

#: ../searx/templates/oscar/results.html:333
#: ../searx/templates/oscar/results.html:367
msgid "next page"
msgstr "Página siguiente"

#: ../searx/templates/oscar/results.html:343
#: ../searx/templates/oscar/results.html:357
msgid "previous page"
msgstr "Página anterior"

#: ../searx/templates/oscar/search.html:6
#: ../searx/templates/oscar/search_full.html:9
msgid "Search for..."
msgstr "Buscar..."

#: ../searx/templates/oscar/search.html:8
#: ../searx/templates/oscar/search_full.html:11
msgid "Start search"
msgstr "Comenzar búsqueda"

#: ../searx/templates/oscar/search.html:9
#: ../searx/templates/oscar/search_full.html:12
msgid "Clear search"
msgstr "Limpiar búsqueda"

#: ../searx/templates/oscar/search_full.html:12
msgid "Clear"
msgstr "Limpiar"

#: ../searx/templates/oscar/stats.html:2
msgid "stats"
msgstr "Estadísitcas"

#: ../searx/templates/oscar/stats.html:5
msgid "Engine stats"
msgstr "Estadísticas del motor de búsqueda"

#: ../searx/templates/oscar/time-range.html:5
msgid "Anytime"
msgstr "En cualquier momento"

#: ../searx/templates/oscar/time-range.html:8
msgid "Last day"
msgstr "Último día"

#: ../searx/templates/oscar/time-range.html:11
msgid "Last week"
msgstr "Última semana"

#: ../searx/templates/oscar/time-range.html:14
msgid "Last month"
msgstr "Último mes"

#: ../searx/templates/oscar/time-range.html:17
msgid "Last year"
msgstr "Último año"

#: ../searx/templates/oscar/messages/first_time.html:6
#: ../searx/templates/oscar/messages/no_data_available.html:3
msgid "Heads up!"
msgstr "¡Atención!"

#: ../searx/templates/oscar/messages/first_time.html:7
msgid "It look like you are using searx first time."
msgstr "Parece que estás usando searx por primera vez."

#: ../searx/templates/oscar/messages/no_cookies.html:3
msgid "Information!"
msgstr "¡Información!"

#: ../searx/templates/oscar/messages/no_cookies.html:4
msgid "currently, there are no cookies defined."
msgstr "No existen cookies definidas actualmente."

#: ../searx/templates/oscar/messages/no_data_available.html:4
msgid "There is currently no data available. "
msgstr "Actualmente no hay datos disponibles."

#: ../searx/templates/oscar/messages/no_results.html:4
msgid "Engines cannot retrieve results."
msgstr "Los motores no pueden obtener resultados."

#: ../searx/templates/oscar/messages/no_results.html:10
msgid "Please, try again later or find another searx instance."
msgstr ""
"Por favor, inténtelo de nuevo más tarde o busque otra instancia de searx."

#: ../searx/templates/oscar/messages/no_results.html:10
msgid "Public instances"
msgstr "Instancias públicas"

#: ../searx/templates/oscar/messages/no_results.html:14
msgid "Sorry!"
msgstr "¡Lo siento!"

#: ../searx/templates/oscar/messages/no_results.html:15
msgid ""
"we didn't find any results. Please use another query or search in more "
"categories."
msgstr ""
"No encontramos nada. Por favor, formule su búsqueda de otra forma o busque "
"en más categorías."

#: ../searx/templates/oscar/messages/save_settings_successfull.html:7
msgid "Well done!"
msgstr "¡Bien hecho!"

#: ../searx/templates/oscar/messages/save_settings_successfull.html:8
msgid "Settings saved successfully."
msgstr "Configuración guardada correctamente."

#: ../searx/templates/oscar/messages/unknow_error.html:7
msgid "Oh snap!"
msgstr "¡Mecachis!"

#: ../searx/templates/oscar/messages/unknow_error.html:8
msgid "Something went wrong."
msgstr "Algo ha ido mal."

#: ../searx/templates/oscar/result_templates/default.html:7
#: ../searx/templates/oscar/result_templates/files.html:7
#: ../searx/templates/oscar/result_templates/files.html:10
msgid "show media"
msgstr "mostrar multimedia"

#: ../searx/templates/oscar/result_templates/default.html:7
#: ../searx/templates/oscar/result_templates/files.html:7
msgid "hide media"
msgstr "ocultar multimedia"

#: ../searx/templates/oscar/result_templates/files.html:33
#: ../searx/templates/oscar/result_templates/videos.html:19
msgid "Author"
msgstr "Autor"

#: ../searx/templates/oscar/result_templates/files.html:35
#, fuzzy
#| msgid "Engine name"
msgid "Filename"
msgstr "Nombre del motor de búsqueda"

#: ../searx/templates/oscar/result_templates/files.html:37
#: ../searx/templates/oscar/result_templates/torrent.html:7
msgid "Filesize"
msgstr "Tamaño de archivo"

#: ../searx/templates/oscar/result_templates/files.html:38
#: ../searx/templates/oscar/result_templates/torrent.html:9
msgid "Bytes"
msgstr "Bytes"

#: ../searx/templates/oscar/result_templates/files.html:39
#: ../searx/templates/oscar/result_templates/torrent.html:10
msgid "kiB"
msgstr "KiB"

#: ../searx/templates/oscar/result_templates/files.html:40
#: ../searx/templates/oscar/result_templates/torrent.html:11
msgid "MiB"
msgstr "MiB"

#: ../searx/templates/oscar/result_templates/files.html:41
#: ../searx/templates/oscar/result_templates/torrent.html:12
msgid "GiB"
msgstr "GiB"

#: ../searx/templates/oscar/result_templates/files.html:42
#: ../searx/templates/oscar/result_templates/torrent.html:13
msgid "TiB"
msgstr "TiB"

#: ../searx/templates/oscar/result_templates/files.html:46
msgid "Date"
msgstr ""

#: ../searx/templates/oscar/result_templates/files.html:48
msgid "Type"
msgstr ""

#: ../searx/templates/oscar/result_templates/images.html:27
msgid "Get image"
msgstr "Obtener imagen"

#: ../searx/templates/oscar/result_templates/images.html:30
msgid "View source"
msgstr "Ver fuente"

#: ../searx/templates/oscar/result_templates/map.html:7
msgid "show map"
msgstr "mostrar mapa"

#: ../searx/templates/oscar/result_templates/map.html:7
msgid "hide map"
msgstr "ocultar mapa"

#: ../searx/templates/oscar/result_templates/map.html:11
msgid "show details"
msgstr "ver detalles"

#: ../searx/templates/oscar/result_templates/map.html:11
msgid "hide details"
msgstr "ocultar detalles"

#: ../searx/templates/oscar/result_templates/torrent.html:6
msgid "Seeder"
msgstr "Fuente"

#: ../searx/templates/oscar/result_templates/torrent.html:6
msgid "Leecher"
msgstr "Descargador"

#: ../searx/templates/oscar/result_templates/torrent.html:15
msgid "Number of Files"
msgstr "Número de archivos"

#: ../searx/templates/oscar/result_templates/videos.html:7
msgid "show video"
msgstr "mostrar vídeo"

#: ../searx/templates/oscar/result_templates/videos.html:7
msgid "hide video"
msgstr "ocultar video"

#: ../searx/templates/oscar/result_templates/videos.html:20
msgid "Length"
msgstr "Longitud"

#~ msgid "CAPTCHA required"
#~ msgstr "CAPTCHA obligatorio"

#~ msgid ""
#~ "Results are opened in the same window by default. This plugin overwrites "
#~ "the default behaviour to open links on new tabs/windows. (JavaScript "
#~ "required)"
#~ msgstr ""
#~ "Los resultados se abren en la misma ventana por defecto. Este plugin "
#~ "sobrescribe el comportamiento por defecto para abrir enlaces en nuevas "
#~ "pestañas / ventanas. (es necesario JavaScript)"

#~ msgid "Color"
#~ msgstr "Color"

#~ msgid "Blue (default)"
#~ msgstr "Azul (predeterminado)"

#~ msgid "Violet"
#~ msgstr "Violeta"

#~ msgid "Green"
#~ msgstr "Verde"

#~ msgid "Cyan"
#~ msgstr "Cian"

#~ msgid "Orange"
#~ msgstr "Naranja"

#~ msgid "Red"
#~ msgstr "Rojo"

#~ msgid "Currently used search engines"
#~ msgstr "Motores de búsqueda actualmente en uso"

#~ msgid "Category"
#~ msgstr "Categoría"

#~ msgid "Block"
#~ msgstr "Bloquear"

#~ msgid "Answers"
#~ msgstr "Respuestas"

#~ msgid "original context"
#~ msgstr "contexto original"

#~ msgid "Click on the magnifier to perform search"
#~ msgstr "Haz clic en la lupa para realizar la búsqueda"

#~ msgid "Powered by"
#~ msgstr "Creado por"

#~ msgid "Keywords"
#~ msgstr "Plabras clave"

#~ msgid "Load more..."
#~ msgstr "Cargar más"

#~ msgid "Supports selected language"
#~ msgstr "Soporta el idioma seleccionado"

#~ msgid "User interface"
#~ msgstr "Interfaz de usuario"

#~ msgid "Privacy"
#~ msgstr "Privacidad"

#~ msgid "Loading..."
#~ msgstr "Cargando..."
