# Translations template for PROJECT.
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
#
# Translators:
# Cymrodor <aled@aledpowell.cymru>, 2019
msgid ""
msgstr ""
"Project-Id-Version: searx\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2021-06-18 02:49+0200\n"
"PO-Revision-Date: 2020-07-09 13:10+0000\n"
"Last-Translator: Adam Tauber <asciimoo@gmail.com>\n"
"Language-Team: Welsh (http://www.transifex.com/asciimoo/searx/language/cy/)\n"
"Language: cy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.7.0\n"
"Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n==2) ? 1 : (n != 8 && n != "
"11) ? 2 : 3;\n"

#: ../searx/webapp.py:164
msgid "files"
msgstr "ffeiliau"

#: ../searx/webapp.py:165
msgid "general"
msgstr "cyffredinol"

#: ../searx/webapp.py:166
msgid "music"
msgstr "cerddoriaeth"

#: ../searx/webapp.py:167
msgid "social media"
msgstr "cyfryngau cymdeithasol"

#: ../searx/webapp.py:168
msgid "images"
msgstr "delweddau"

#: ../searx/webapp.py:169
msgid "videos"
msgstr "fideos"

#: ../searx/webapp.py:170
msgid "it"
msgstr "Technoleg"

#: ../searx/webapp.py:171
msgid "news"
msgstr "newyddion"

#: ../searx/webapp.py:172
msgid "map"
msgstr "map"

#: ../searx/webapp.py:173
msgid "onions"
msgstr ""

#: ../searx/webapp.py:174
msgid "science"
msgstr "gwyddoniaeth"

#: ../searx/webapp.py:375
msgid "could not load data"
msgstr ""

#: ../searx/webapp.py:377
msgid "No item found"
msgstr "Ni chanfuwyd eitem"

#: ../searx/webapp.py:483 ../searx/webapp.py:848
msgid "Invalid settings, please edit your preferences"
msgstr "Gosodiadau annilys. Addasa dy ddewisiadau."

#: ../searx/webapp.py:499
msgid "Invalid settings"
msgstr "Gosodiadau annilys"

#: ../searx/webapp.py:564 ../searx/webapp.py:629
msgid "search error"
msgstr "gwall chwilio"

#: ../searx/webapp.py:672
msgid "{minutes} minute(s) ago"
msgstr "{minutes} munud yn ôl"

#: ../searx/webapp.py:674
msgid "{hours} hour(s), {minutes} minute(s) ago"
msgstr "{hours} awr, {minutes} munud yn ôl"

#: ../searx/answerers/statistics/answerer.py:54
msgid "Statistics functions"
msgstr ""

#: ../searx/answerers/statistics/answerer.py:55
msgid "Compute {functions} of the arguments"
msgstr ""

#: ../searx/engines/__init__.py:251
msgid "Engine time (sec)"
msgstr ""

#: ../searx/engines/__init__.py:255
msgid "Page loads (sec)"
msgstr ""

#: ../searx/engines/__init__.py:259 ../searx/templates/oscar/results.html:34
msgid "Number of results"
msgstr "Nifer o ganlyniadau"

#: ../searx/engines/__init__.py:263
msgid "Scores"
msgstr "Sgoriau"

#: ../searx/engines/__init__.py:267
msgid "Scores per result"
msgstr ""

#: ../searx/engines/__init__.py:271
msgid "Errors"
msgstr "Gwallau"

#: ../searx/engines/openstreetmap.py:49
msgid "Get directions"
msgstr ""

#: ../searx/engines/pdbe.py:90
msgid "{title} (OBSOLETE)"
msgstr ""

#: ../searx/engines/pdbe.py:97
msgid "This entry has been superseded by"
msgstr ""

#: ../searx/engines/pubmed.py:78
msgid "No abstract is available for this publication."
msgstr ""

#: ../searx/plugins/better_answerer.py:9
#, fuzzy
#| msgid "Answerers"
msgid "Better Answerers"
msgstr "Atebwyr"

#: ../searx/plugins/better_answerer.py:10
msgid "Module to improve the Answerer functionality in searx"
msgstr ""

#: ../searx/plugins/hostname_replace.py:7
msgid "Hostname replace"
msgstr ""

#: ../searx/plugins/hostname_replace.py:8
msgid "Rewrite result hostnames"
msgstr ""

#: ../searx/plugins/https_rewrite.py:29
msgid "Rewrite HTTP links to HTTPS if possible"
msgstr ""

#: ../searx/plugins/infinite_scroll.py:3
msgid "Infinite scroll"
msgstr ""

#: ../searx/plugins/infinite_scroll.py:4
msgid "Automatically load next page when scrolling to bottom of current page"
msgstr ""

#: ../searx/plugins/oa_doi_rewrite.py:9
msgid "Open Access DOI rewrite"
msgstr ""

#: ../searx/plugins/oa_doi_rewrite.py:10
msgid ""
"Avoid paywalls by redirecting to open-access versions of publications when "
"available"
msgstr ""

#: ../searx/plugins/search_on_category_select.py:18
msgid "Search on category select"
msgstr ""

#: ../searx/plugins/search_on_category_select.py:19
msgid ""
"Perform search immediately if a category selected. Disable to select "
"multiple categories. (JavaScript required)"
msgstr ""

#: ../searx/plugins/self_info.py:19
#, fuzzy
#| msgid "Information!"
msgid "Self Informations"
msgstr "Gwybodaeth!"

#: ../searx/plugins/self_info.py:20
msgid ""
"Displays your IP if the query is \"ip\" and your user agent if the query "
"contains \"user agent\"."
msgstr ""

#: ../searx/plugins/tracker_url_remover.py:27
msgid "Tracker URL remover"
msgstr ""

#: ../searx/plugins/tracker_url_remover.py:28
msgid "Remove trackers arguments from the returned URL"
msgstr ""

#: ../searx/plugins/vim_hotkeys.py:3
msgid "Vim-like hotkeys"
msgstr ""

#: ../searx/plugins/vim_hotkeys.py:4
msgid ""
"Navigate search results with Vim-like hotkeys (JavaScript required). Press "
"\"h\" key on main or result page to get help."
msgstr ""

#: ../searx/plugins/answerer/alternative/answerer.py:24
msgid "The ToS;DR Team"
msgstr ""

#: ../searx/plugins/answerer/alternative/answerer.py:85
msgid "Privacy Alternative"
msgstr ""

#: ../searx/plugins/answerer/alternative/answerer.py:86
msgid "Get privacy friendly alternatives from popular services."
msgstr ""

#: ../searx/plugins/answerer/alternative/answerer.py:90
msgid "Report a bug to the maintainer"
msgstr ""

#: ../searx/plugins/answerer/alternative/templates/answer.html:1
msgid "Privacy friendly alternatives to {service}"
msgstr ""

#: ../searx/plugins/answerer/ascii/answerer.py:41
#, fuzzy
#| msgid "Shortcut"
msgid "Ascii Shortcuts"
msgstr "Llwybr Byr"

#: ../searx/plugins/answerer/ascii/answerer.py:42
msgid "Does some Ascii Stuff"
msgstr ""

#: ../searx/plugins/answerer/bitcoin/answerer.py:96
msgid "Bitcoin Index"
msgstr ""

#: ../searx/plugins/answerer/bitcoin/answerer.py:97
msgid "Get a the bitcoin index via currency or by default via USD, EUR and GBP"
msgstr ""

#: ../searx/plugins/answerer/bitcoin/templates/all.html:1
#: ../searx/plugins/answerer/bitcoin/templates/currency.html:1
msgid "The current Bitcoin index is"
msgstr ""

#: ../searx/plugins/answerer/cheatsheets/answerer.py:79
msgid "Cheatsheet Index"
msgstr ""

#: ../searx/plugins/answerer/cheatsheets/answerer.py:80
msgid "Cheatsheet module to load some awesome modules found around the web."
msgstr ""

#: ../searx/plugins/answerer/cheatsheets/modules/guitar/templates/all.html:1
msgid "Guitar Cheatsheet"
msgstr ""

#: ../searx/plugins/answerer/cheatsheets/modules/guitar/templates/all.html:24
msgid "Cheatsheet Images by {source}"
msgstr ""

#: ../searx/plugins/answerer/grade/answerer.py:82
msgid "{service} has a Privacy {grade} on ToS;DR"
msgstr ""

#: ../searx/plugins/answerer/grade/answerer.py:116
msgid "ToS;DR Grade"
msgstr ""

#: ../searx/plugins/answerer/grade/answerer.py:117
msgid "Get a grade from ToS;DR using the ToS;DR API"
msgstr ""

#: ../searx/plugins/answerer/hash/answerer.py:69
msgid "Hash Generator"
msgstr ""

#: ../searx/plugins/answerer/hash/answerer.py:70
msgid "Generate md4, md5, sha1, sha256 and sha512 hashes"
msgstr ""

#: ../searx/plugins/answerer/leetspeak/answerer.py:64
msgid "Leetspeak Answerer"
msgstr ""

#: ../searx/plugins/answerer/leetspeak/answerer.py:65
msgid "L 3 3 '][' 5 |D 3 /-\\ |< I 5 C 0 0 |_"
msgstr ""

#: ../searx/plugins/answerer/nato/answerer.py:66
msgid "Nato Converter"
msgstr ""

#: ../searx/plugins/answerer/nato/answerer.py:67
msgid "Converts words into the nato phonetic alphabet"
msgstr ""

#: ../searx/plugins/answerer/onion/answerer.py:40
msgid "Onion Notice"
msgstr ""

#: ../searx/plugins/answerer/onion/answerer.py:41
msgid "Give a notice if an onion address has been queried"
msgstr ""

#: ../searx/plugins/answerer/random/answerer.py:94
msgid "Random value generator"
msgstr ""

#: ../searx/plugins/answerer/random/answerer.py:95
msgid "Generate different random values"
msgstr ""

#: ../searx/templates/oscar/404.html:4
msgid "Page not found"
msgstr ""

#: ../searx/templates/oscar/404.html:6
#, python-format
msgid "Go to %(search_page)s."
msgstr "Mynd i %(search_page)s."

#: ../searx/templates/oscar/404.html:6
msgid "search page"
msgstr "tudalen chwilio"

#: ../searx/templates/oscar/about.html:2 ../searx/templates/oscar/navbar.html:6
msgid "about"
msgstr "ynghylch"

#: ../searx/templates/oscar/advanced.html:4
msgid "Advanced settings"
msgstr "Gosodiadau uwch"

#: ../searx/templates/oscar/base.html:77
#: ../searx/templates/oscar/messages/first_time.html:4
#: ../searx/templates/oscar/messages/save_settings_successfull.html:5
#: ../searx/templates/oscar/messages/unknow_error.html:5
msgid "Close"
msgstr "Cau"

#: ../searx/templates/oscar/base.html:79
#: ../searx/templates/oscar/messages/no_results.html:4
msgid "Error!"
msgstr "Gwall!"

#: ../searx/templates/oscar/base.html:107
msgid "Powered by {searx} and forked by {tosdr}"
msgstr ""

#: ../searx/templates/oscar/base.html:108
msgid "{version} on commit {hash}"
msgstr ""

#: ../searx/templates/oscar/base.html:109
msgid "a privacy-respecting, hackable metasearch engine"
msgstr ""

#: ../searx/templates/oscar/base.html:110
msgid "Source code"
msgstr ""

#: ../searx/templates/oscar/base.html:111
msgid "Issue tracker"
msgstr ""

#: ../searx/templates/oscar/base.html:113
msgid "Contact instance maintainer"
msgstr ""

#: ../searx/templates/oscar/base.html:115
msgid "Translate ToS;DR Search"
msgstr ""

#: ../searx/templates/oscar/languages.html:2
msgid "Language"
msgstr ""

#: ../searx/templates/oscar/languages.html:4
msgid "Default language"
msgstr "Iaith arferol"

#: ../searx/templates/oscar/macros.html:23
msgid "magnet link"
msgstr "dolen magnet"

#: ../searx/templates/oscar/macros.html:24
msgid "torrent file"
msgstr "ffeil torrent"

#: ../searx/templates/oscar/macros.html:36
#: ../searx/templates/oscar/macros.html:38
#: ../searx/templates/oscar/macros.html:72
#: ../searx/templates/oscar/macros.html:74
msgid "cached"
msgstr ""

#: ../searx/templates/oscar/macros.html:42
#: ../searx/templates/oscar/macros.html:58
#: ../searx/templates/oscar/macros.html:78
#: ../searx/templates/oscar/macros.html:92
msgid "proxied"
msgstr ""

#: ../searx/templates/oscar/macros.html:132
#: ../searx/templates/oscar/preferences.html:252
#: ../searx/templates/oscar/preferences.html:269
msgid "Allow"
msgstr "Caniatáu"

#: ../searx/templates/oscar/macros.html:139
msgid "supported"
msgstr "cefnogir"

#: ../searx/templates/oscar/macros.html:143
msgid "not supported"
msgstr "ni chefnogir"

#: ../searx/templates/oscar/navbar.html:7
#: ../searx/templates/oscar/preferences.html:16
msgid "preferences"
msgstr "dewisiadau"

#: ../searx/templates/oscar/preferences.html:12
msgid "No HTTPS"
msgstr ""

#: ../searx/templates/oscar/preferences.html:21
msgid "Preferences"
msgstr "Dewisiadau"

#: ../searx/templates/oscar/preferences.html:26
#: ../searx/templates/oscar/preferences.html:36
msgid "General"
msgstr "Cyffredin"

#: ../searx/templates/oscar/preferences.html:27
#: ../searx/templates/oscar/preferences.html:226
msgid "Engines"
msgstr "Peiriannau"

#: ../searx/templates/oscar/preferences.html:28
#: ../searx/templates/oscar/preferences.html:329
msgid "Plugins"
msgstr "Ategolion"

#: ../searx/templates/oscar/preferences.html:30
#: ../searx/templates/oscar/preferences.html:359
msgid "Answerers"
msgstr "Atebwyr"

#: ../searx/templates/oscar/preferences.html:31
#: ../searx/templates/oscar/preferences.html:412
msgid "Cookies"
msgstr "Cwcis"

#: ../searx/templates/oscar/preferences.html:49
#: ../searx/templates/oscar/preferences.html:52
msgid "Default categories"
msgstr "Categorïau arferol"

#: ../searx/templates/oscar/preferences.html:60
msgid "Search language"
msgstr "Iaith chwilio"

#: ../searx/templates/oscar/preferences.html:61
msgid "What language do you prefer for search?"
msgstr "Ym mha iaith wyt ti'n ffafrio chwilio?"

#: ../searx/templates/oscar/preferences.html:68
msgid "Interface language"
msgstr "Iaith y rhyngwyneb"

#: ../searx/templates/oscar/preferences.html:69
msgid "Change the language of the layout"
msgstr "Newid iaith rhyngwyneb searX"

#: ../searx/templates/oscar/preferences.html:81
msgid "Autocomplete"
msgstr "Awto-gwblhau"

#: ../searx/templates/oscar/preferences.html:82
msgid "Find stuff as you type"
msgstr "Darganfod pethau wrth i chi deipio"

#: ../searx/templates/oscar/preferences.html:96
msgid "Image proxy"
msgstr ""

#: ../searx/templates/oscar/preferences.html:97
msgid "Proxying image results through searx"
msgstr ""

#: ../searx/templates/oscar/preferences.html:102
msgid "Enabled"
msgstr "Galluogwyd"

#: ../searx/templates/oscar/preferences.html:104
msgid "Disabled"
msgstr "Analluogwyd"

#: ../searx/templates/oscar/preferences.html:110
msgid "Method"
msgstr "Dull"

#: ../searx/templates/oscar/preferences.html:111
msgid ""
"Change how forms are submited, <a href=\"http://en.wikipedia.org/wiki/"
"Hypertext_Transfer_Protocol#Request_methods\" rel=\"external\">learn more "
"about request methods</a>"
msgstr ""

#: ../searx/templates/oscar/preferences.html:123
#: ../searx/templates/oscar/preferences.html:256
#: ../searx/templates/oscar/preferences.html:264
msgid "SafeSearch"
msgstr ""

#: ../searx/templates/oscar/preferences.html:124
msgid "Filter content"
msgstr ""

#: ../searx/templates/oscar/preferences.html:129
msgid "Strict"
msgstr "Caeth"

#: ../searx/templates/oscar/preferences.html:131
msgid "Moderate"
msgstr "Cymhedrol"

#: ../searx/templates/oscar/preferences.html:133
msgid "None"
msgstr "Dim"

#: ../searx/templates/oscar/preferences.html:139
msgid "Themes"
msgstr "Themâu"

#: ../searx/templates/oscar/preferences.html:140
msgid "Change searx layout"
msgstr "Newid cynllun searX"

#: ../searx/templates/oscar/preferences.html:152
#: ../searx/templates/oscar/preferences.html:160
msgid "Choose style for this theme"
msgstr "Dewis arddull ar gyfer y thema hon"

#: ../searx/templates/oscar/preferences.html:152
#: ../searx/templates/oscar/preferences.html:160
msgid "Style"
msgstr "Arddull"

#: ../searx/templates/oscar/preferences.html:164
msgid "Results on new tabs"
msgstr "Canlyniadau mewn tabiau newydd"

#: ../searx/templates/oscar/preferences.html:165
msgid "Open result links on new browser tabs"
msgstr "Agor dolenni canlyniadau mewn tabiau newydd yn y porwr"

#: ../searx/templates/oscar/preferences.html:170
#: ../searx/templates/oscar/preferences.html:183
msgid "On"
msgstr "Ymlaen"

#: ../searx/templates/oscar/preferences.html:172
#: ../searx/templates/oscar/preferences.html:185
msgid "Off"
msgstr "I ffwrdd"

#: ../searx/templates/oscar/preferences.html:177
#, fuzzy
#| msgid "Advanced settings"
msgid "Show advanced settings"
msgstr "Gosodiadau uwch"

#: ../searx/templates/oscar/preferences.html:178
msgid "Show advanced settings panel in the home page by default"
msgstr ""

#: ../searx/templates/oscar/preferences.html:190
msgid "Open Access DOI resolver"
msgstr ""

#: ../searx/templates/oscar/preferences.html:191
msgid ""
"Redirect to open-access versions of publications when available (plugin "
"required)"
msgstr ""

#: ../searx/templates/oscar/preferences.html:205
msgid "Engine tokens"
msgstr ""

#: ../searx/templates/oscar/preferences.html:206
msgid "Access tokens for private engines"
msgstr ""

#: ../searx/templates/oscar/preferences.html:235
msgid "Allow all"
msgstr ""

#: ../searx/templates/oscar/preferences.html:237
msgid "Disable all"
msgstr ""

#: ../searx/templates/oscar/preferences.html:253
#: ../searx/templates/oscar/preferences.html:268
msgid "Engine name"
msgstr ""

#: ../searx/templates/oscar/preferences.html:254
#: ../searx/templates/oscar/preferences.html:267
msgid "Shortcut"
msgstr "Llwybr Byr"

#: ../searx/templates/oscar/preferences.html:255
#: ../searx/templates/oscar/preferences.html:266
msgid "Selected language"
msgstr "Iaith a ddewiswyd"

#: ../searx/templates/oscar/preferences.html:257
#: ../searx/templates/oscar/preferences.html:263
#: ../searx/templates/oscar/time-range.html:2
msgid "Time range"
msgstr "Cyfnod amser"

#: ../searx/templates/oscar/preferences.html:258
#: ../searx/templates/oscar/preferences.html:262
msgid "Avg. time"
msgstr ""

#: ../searx/templates/oscar/preferences.html:259
#: ../searx/templates/oscar/preferences.html:261
msgid "Max time"
msgstr ""

#: ../searx/templates/oscar/preferences.html:362
msgid "This is the list of searx's instant answering modules."
msgstr ""

#: ../searx/templates/oscar/preferences.html:366
msgid "Name"
msgstr "Enw"

#: ../searx/templates/oscar/preferences.html:367
#: ../searx/templates/oscar/results.html:191
#: ../searx/templates/oscar/results.html:196
#: ../searx/templates/oscar/results.html:204
#: ../searx/templates/oscar/results.html:209
msgid "Developer"
msgstr ""

#: ../searx/templates/oscar/preferences.html:368
msgid "Description"
msgstr "Disgrifiad"

#: ../searx/templates/oscar/preferences.html:369
msgid "Examples"
msgstr "Enghreifftiau"

#: ../searx/templates/oscar/preferences.html:415
msgid ""
"This is the list of cookies and their values searx is storing on your "
"computer."
msgstr ""
"Dyma restr y cwcis, a'u gwerthoedd, mae searX yn eu cadw ar eich dyfais."

#: ../searx/templates/oscar/preferences.html:416
msgid "With that list, you can assess searx transparency."
msgstr ""

#: ../searx/templates/oscar/preferences.html:421
msgid "Cookie name"
msgstr "Enw cwci"

#: ../searx/templates/oscar/preferences.html:422
msgid "Value"
msgstr "Gwerth"

#: ../searx/templates/oscar/preferences.html:439
msgid ""
"These settings are stored in your cookies, this allows us not to store this "
"data about you."
msgstr ""

#: ../searx/templates/oscar/preferences.html:440
msgid ""
"These cookies serve your sole convenience, we don't use these cookies to "
"track you."
msgstr ""

#: ../searx/templates/oscar/preferences.html:444
msgid "Search URL of the currently saved preferences"
msgstr ""

#: ../searx/templates/oscar/preferences.html:445
msgid ""
"Note: specifying custom settings in the search URL can reduce privacy by "
"leaking data to the clicked result sites."
msgstr ""

#: ../searx/templates/oscar/preferences.html:452
msgid "save"
msgstr "cadw"

#: ../searx/templates/oscar/preferences.html:454
msgid "back"
msgstr "nôl"

#: ../searx/templates/oscar/preferences.html:457
msgid "Reset defaults"
msgstr "Ailosod rhagosodiadau"

#: ../searx/templates/oscar/results.html:39
msgid "Engines cannot retrieve results"
msgstr "Ni all y peiriannau cael canlyniadau"

#: ../searx/templates/oscar/results.html:55
msgid "Suggestions"
msgstr "Awgrymiadau"

#: ../searx/templates/oscar/results.html:77
msgid "Links"
msgstr "Dolenni"

#: ../searx/templates/oscar/results.html:82
msgid "Search URL"
msgstr ""

#: ../searx/templates/oscar/results.html:88
msgid "Download results"
msgstr "Lawrlwytho'r canlyniadau"

#: ../searx/templates/oscar/results.html:100
msgid "RSS subscription"
msgstr ""

#: ../searx/templates/oscar/results.html:107
msgid "Search results"
msgstr "Canlyniadau chwilio"

#: ../searx/templates/oscar/results.html:112
msgid "Try searching for:"
msgstr "Rho gynnig ar chwilio am:"

#: ../searx/templates/oscar/results.html:159
msgid "No Description provided by the developer"
msgstr ""

#: ../searx/templates/oscar/results.html:166
msgid "Issue Tracker"
msgstr ""

#: ../searx/templates/oscar/results.html:175
msgid ""
"Source\n"
"                                                        Code"
msgstr ""

#: ../searx/templates/oscar/results.html:183
msgid "Website"
msgstr ""

#: ../searx/templates/oscar/results.html:253
#: ../searx/templates/oscar/results.html:283
#: ../searx/templates/oscar/results.html:296
msgid "Missing Text"
msgstr ""

#: ../searx/templates/oscar/results.html:333
#: ../searx/templates/oscar/results.html:367
msgid "next page"
msgstr "tudalen nesaf"

#: ../searx/templates/oscar/results.html:343
#: ../searx/templates/oscar/results.html:357
msgid "previous page"
msgstr "tudalen ddiwethaf"

#: ../searx/templates/oscar/search.html:6
#: ../searx/templates/oscar/search_full.html:9
msgid "Search for..."
msgstr "Chwilio am..."

#: ../searx/templates/oscar/search.html:8
#: ../searx/templates/oscar/search_full.html:11
msgid "Start search"
msgstr "Dechrau chwilio"

#: ../searx/templates/oscar/search.html:9
#: ../searx/templates/oscar/search_full.html:12
msgid "Clear search"
msgstr ""

#: ../searx/templates/oscar/search_full.html:12
msgid "Clear"
msgstr ""

#: ../searx/templates/oscar/stats.html:2
msgid "stats"
msgstr "ystadegau"

#: ../searx/templates/oscar/stats.html:5
msgid "Engine stats"
msgstr ""

#: ../searx/templates/oscar/time-range.html:5
msgid "Anytime"
msgstr "Unrhyw amser"

#: ../searx/templates/oscar/time-range.html:8
msgid "Last day"
msgstr "Y diwrnod diwethaf"

#: ../searx/templates/oscar/time-range.html:11
msgid "Last week"
msgstr "Yr wythnos diwethaf"

#: ../searx/templates/oscar/time-range.html:14
msgid "Last month"
msgstr "Y mis diwethaf"

#: ../searx/templates/oscar/time-range.html:17
msgid "Last year"
msgstr "Y flwyddyn ddiwethaf"

#: ../searx/templates/oscar/messages/first_time.html:6
#: ../searx/templates/oscar/messages/no_data_available.html:3
msgid "Heads up!"
msgstr ""

#: ../searx/templates/oscar/messages/first_time.html:7
msgid "It look like you are using searx first time."
msgstr "Mae'n ymddangos eich bod yn defnyddio searx am y tro cyntaf."

#: ../searx/templates/oscar/messages/no_cookies.html:3
msgid "Information!"
msgstr "Gwybodaeth!"

#: ../searx/templates/oscar/messages/no_cookies.html:4
msgid "currently, there are no cookies defined."
msgstr ""

#: ../searx/templates/oscar/messages/no_data_available.html:4
msgid "There is currently no data available. "
msgstr "Does dim data ar gael ar hyn o bryd."

#: ../searx/templates/oscar/messages/no_results.html:4
msgid "Engines cannot retrieve results."
msgstr "Ni all y peiriannau cael canlyniadau."

#: ../searx/templates/oscar/messages/no_results.html:10
msgid "Please, try again later or find another searx instance."
msgstr ""

#: ../searx/templates/oscar/messages/no_results.html:10
msgid "Public instances"
msgstr ""

#: ../searx/templates/oscar/messages/no_results.html:14
msgid "Sorry!"
msgstr "Sori!"

#: ../searx/templates/oscar/messages/no_results.html:15
msgid ""
"we didn't find any results. Please use another query or search in more "
"categories."
msgstr ""
"Ni ddaethpwyd o hyd i unrhyw ganlyniadau. Defnyddiwch derm(au) chwilio "
"gwahanol neu ehangu'r chwilio i ragor o gategorïau."

#: ../searx/templates/oscar/messages/save_settings_successfull.html:7
msgid "Well done!"
msgstr "Da iawn!"

#: ../searx/templates/oscar/messages/save_settings_successfull.html:8
msgid "Settings saved successfully."
msgstr "Cadwyd y gosodiadau yn iawn!"

#: ../searx/templates/oscar/messages/unknow_error.html:7
msgid "Oh snap!"
msgstr ""

#: ../searx/templates/oscar/messages/unknow_error.html:8
msgid "Something went wrong."
msgstr "Aeth rhywbeth o'i le."

#: ../searx/templates/oscar/result_templates/default.html:7
#: ../searx/templates/oscar/result_templates/files.html:7
#: ../searx/templates/oscar/result_templates/files.html:10
msgid "show media"
msgstr "dangos cyfryngau"

#: ../searx/templates/oscar/result_templates/default.html:7
#: ../searx/templates/oscar/result_templates/files.html:7
msgid "hide media"
msgstr "cuddio cyfryngau"

#: ../searx/templates/oscar/result_templates/files.html:33
#: ../searx/templates/oscar/result_templates/videos.html:19
msgid "Author"
msgstr ""

#: ../searx/templates/oscar/result_templates/files.html:35
#, fuzzy
#| msgid "Cookie name"
msgid "Filename"
msgstr "Enw cwci"

#: ../searx/templates/oscar/result_templates/files.html:37
#: ../searx/templates/oscar/result_templates/torrent.html:7
msgid "Filesize"
msgstr "Maint ffeil"

#: ../searx/templates/oscar/result_templates/files.html:38
#: ../searx/templates/oscar/result_templates/torrent.html:9
msgid "Bytes"
msgstr "Beitiau"

#: ../searx/templates/oscar/result_templates/files.html:39
#: ../searx/templates/oscar/result_templates/torrent.html:10
msgid "kiB"
msgstr "kiB"

#: ../searx/templates/oscar/result_templates/files.html:40
#: ../searx/templates/oscar/result_templates/torrent.html:11
msgid "MiB"
msgstr "MiB"

#: ../searx/templates/oscar/result_templates/files.html:41
#: ../searx/templates/oscar/result_templates/torrent.html:12
msgid "GiB"
msgstr "GiB"

#: ../searx/templates/oscar/result_templates/files.html:42
#: ../searx/templates/oscar/result_templates/torrent.html:13
msgid "TiB"
msgstr "TiB"

#: ../searx/templates/oscar/result_templates/files.html:46
msgid "Date"
msgstr ""

#: ../searx/templates/oscar/result_templates/files.html:48
msgid "Type"
msgstr ""

#: ../searx/templates/oscar/result_templates/images.html:27
msgid "Get image"
msgstr "Cael y ddelwedd"

#: ../searx/templates/oscar/result_templates/images.html:30
msgid "View source"
msgstr "Gweld y ffynhonnell"

#: ../searx/templates/oscar/result_templates/map.html:7
msgid "show map"
msgstr "dangos map"

#: ../searx/templates/oscar/result_templates/map.html:7
msgid "hide map"
msgstr "cuddio map"

#: ../searx/templates/oscar/result_templates/map.html:11
msgid "show details"
msgstr "dangos manylion"

#: ../searx/templates/oscar/result_templates/map.html:11
msgid "hide details"
msgstr "cuddio manylion"

#: ../searx/templates/oscar/result_templates/torrent.html:6
msgid "Seeder"
msgstr "Hadau"

#: ../searx/templates/oscar/result_templates/torrent.html:6
msgid "Leecher"
msgstr "Lawrlwythwyr"

#: ../searx/templates/oscar/result_templates/torrent.html:15
msgid "Number of Files"
msgstr "Nifer o Ffeiliau"

#: ../searx/templates/oscar/result_templates/videos.html:7
msgid "show video"
msgstr "dangos fideo"

#: ../searx/templates/oscar/result_templates/videos.html:7
msgid "hide video"
msgstr "cuddio fideo"

#: ../searx/templates/oscar/result_templates/videos.html:20
msgid "Length"
msgstr ""

#~ msgid ""
#~ "Results are opened in the same window by default. This plugin overwrites "
#~ "the default behaviour to open links on new tabs/windows. (JavaScript "
#~ "required)"
#~ msgstr ""
#~ "Mae canlyniadau fel arfer yn cael eu hagor yn yr un ffenestr. Mae'r "
#~ "ategolyn hwn yn newid hyn fel bod dolenni yn cael eu hagor mewn tabiau/"
#~ "ffenestri newydd. (Angen JavaScript)"

#~ msgid "Color"
#~ msgstr "Lliw"

#~ msgid "Blue (default)"
#~ msgstr "Glas (arferol)"

#~ msgid "Violet"
#~ msgstr "Fioled"

#~ msgid "Green"
#~ msgstr "Gwyrdd"

#~ msgid "Cyan"
#~ msgstr "Gwyrddlas"

#~ msgid "Orange"
#~ msgstr "Oren"

#~ msgid "Red"
#~ msgstr "Coch"

#~ msgid "Category"
#~ msgstr "Categori"

#~ msgid "Block"
#~ msgstr "Rhwystro"

#~ msgid "Answers"
#~ msgstr "Atebion"

#~ msgid "original context"
#~ msgstr "cyd-destun gwreiddiol"

#~ msgid "Click on the magnifier to perform search"
#~ msgstr "Cliciwch ar y chwyddwydr i berfformio chwiliad"

#~ msgid "Powered by"
#~ msgstr "Pwerwyd gan"

#~ msgid "Keywords"
#~ msgstr "Allweddeiriau"

#~ msgid "Load more..."
#~ msgstr "Dysgu mwy..."

#~ msgid "Supports selected language"
#~ msgstr "Cefnogir yr iaith a ddewiswyd"

#~ msgid "User interface"
#~ msgstr "Rhyngwyneb defnyddiwr"

#~ msgid "Privacy"
#~ msgstr "Preifatrwydd"
