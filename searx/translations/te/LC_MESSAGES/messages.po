# Translations template for PROJECT.
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
#
# Translators:
# Joseph Nuthalapati <njoseph@thoughtworks.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: searx\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2021-06-18 02:49+0200\n"
"PO-Revision-Date: 2020-07-09 13:10+0000\n"
"Last-Translator: Adam Tauber <asciimoo@gmail.com>\n"
"Language-Team: Telugu (http://www.transifex.com/asciimoo/searx/language/"
"te/)\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.7.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../searx/webapp.py:164
msgid "files"
msgstr "ఫైళ్ళు"

#: ../searx/webapp.py:165
msgid "general"
msgstr "సాధారణ"

#: ../searx/webapp.py:166
msgid "music"
msgstr "సంగీతం"

#: ../searx/webapp.py:167
msgid "social media"
msgstr "సోషల్ మీడియా"

#: ../searx/webapp.py:168
msgid "images"
msgstr "చిత్రాలు"

#: ../searx/webapp.py:169
msgid "videos"
msgstr "వీడియోలు"

#: ../searx/webapp.py:170
msgid "it"
msgstr "ఐటి"

#: ../searx/webapp.py:171
msgid "news"
msgstr "వార్తలు"

#: ../searx/webapp.py:172
msgid "map"
msgstr "పటము"

#: ../searx/webapp.py:173
msgid "onions"
msgstr ""

#: ../searx/webapp.py:174
msgid "science"
msgstr "విజ్ఞానశాస్త్రం"

#: ../searx/webapp.py:375
msgid "could not load data"
msgstr ""

#: ../searx/webapp.py:377
msgid "No item found"
msgstr "ఏమీ దొరకలేదు"

#: ../searx/webapp.py:483 ../searx/webapp.py:848
msgid "Invalid settings, please edit your preferences"
msgstr ""

#: ../searx/webapp.py:499
msgid "Invalid settings"
msgstr "చెల్లని అమరికలు"

#: ../searx/webapp.py:564 ../searx/webapp.py:629
msgid "search error"
msgstr "శోధనలో దోషము"

#: ../searx/webapp.py:672
msgid "{minutes} minute(s) ago"
msgstr "{minutes} నిమిషము(ల) క్రిందట"

#: ../searx/webapp.py:674
msgid "{hours} hour(s), {minutes} minute(s) ago"
msgstr ""

#: ../searx/answerers/statistics/answerer.py:54
msgid "Statistics functions"
msgstr "సాంఖ్యకశాస్త్ర ప్రమేయాలు"

#: ../searx/answerers/statistics/answerer.py:55
msgid "Compute {functions} of the arguments"
msgstr ""

#: ../searx/engines/__init__.py:251
msgid "Engine time (sec)"
msgstr ""

#: ../searx/engines/__init__.py:255
msgid "Page loads (sec)"
msgstr ""

#: ../searx/engines/__init__.py:259 ../searx/templates/oscar/results.html:34
msgid "Number of results"
msgstr "ఫలితముల సంఖ్య"

#: ../searx/engines/__init__.py:263
msgid "Scores"
msgstr ""

#: ../searx/engines/__init__.py:267
msgid "Scores per result"
msgstr ""

#: ../searx/engines/__init__.py:271
msgid "Errors"
msgstr "దోషములు"

#: ../searx/engines/openstreetmap.py:49
msgid "Get directions"
msgstr ""

#: ../searx/engines/pdbe.py:90
msgid "{title} (OBSOLETE)"
msgstr ""

#: ../searx/engines/pdbe.py:97
msgid "This entry has been superseded by"
msgstr ""

#: ../searx/engines/pubmed.py:78
msgid "No abstract is available for this publication."
msgstr ""

#: ../searx/plugins/better_answerer.py:9
#, fuzzy
#| msgid "Answerers"
msgid "Better Answerers"
msgstr "జవాబులు"

#: ../searx/plugins/better_answerer.py:10
msgid "Module to improve the Answerer functionality in searx"
msgstr ""

#: ../searx/plugins/hostname_replace.py:7
msgid "Hostname replace"
msgstr ""

#: ../searx/plugins/hostname_replace.py:8
msgid "Rewrite result hostnames"
msgstr ""

#: ../searx/plugins/https_rewrite.py:29
msgid "Rewrite HTTP links to HTTPS if possible"
msgstr ""

#: ../searx/plugins/infinite_scroll.py:3
msgid "Infinite scroll"
msgstr ""

#: ../searx/plugins/infinite_scroll.py:4
msgid "Automatically load next page when scrolling to bottom of current page"
msgstr ""

#: ../searx/plugins/oa_doi_rewrite.py:9
msgid "Open Access DOI rewrite"
msgstr ""

#: ../searx/plugins/oa_doi_rewrite.py:10
msgid ""
"Avoid paywalls by redirecting to open-access versions of publications when "
"available"
msgstr ""

#: ../searx/plugins/search_on_category_select.py:18
msgid "Search on category select"
msgstr ""

#: ../searx/plugins/search_on_category_select.py:19
msgid ""
"Perform search immediately if a category selected. Disable to select "
"multiple categories. (JavaScript required)"
msgstr ""

#: ../searx/plugins/self_info.py:19
#, fuzzy
#| msgid "Information!"
msgid "Self Informations"
msgstr "సమాచారం!"

#: ../searx/plugins/self_info.py:20
msgid ""
"Displays your IP if the query is \"ip\" and your user agent if the query "
"contains \"user agent\"."
msgstr ""

#: ../searx/plugins/tracker_url_remover.py:27
msgid "Tracker URL remover"
msgstr ""

#: ../searx/plugins/tracker_url_remover.py:28
msgid "Remove trackers arguments from the returned URL"
msgstr ""

#: ../searx/plugins/vim_hotkeys.py:3
msgid "Vim-like hotkeys"
msgstr ""

#: ../searx/plugins/vim_hotkeys.py:4
msgid ""
"Navigate search results with Vim-like hotkeys (JavaScript required). Press "
"\"h\" key on main or result page to get help."
msgstr ""

#: ../searx/plugins/answerer/alternative/answerer.py:24
msgid "The ToS;DR Team"
msgstr ""

#: ../searx/plugins/answerer/alternative/answerer.py:85
msgid "Privacy Alternative"
msgstr ""

#: ../searx/plugins/answerer/alternative/answerer.py:86
msgid "Get privacy friendly alternatives from popular services."
msgstr ""

#: ../searx/plugins/answerer/alternative/answerer.py:90
msgid "Report a bug to the maintainer"
msgstr ""

#: ../searx/plugins/answerer/alternative/templates/answer.html:1
msgid "Privacy friendly alternatives to {service}"
msgstr ""

#: ../searx/plugins/answerer/ascii/answerer.py:41
#, fuzzy
#| msgid "Shortcut"
msgid "Ascii Shortcuts"
msgstr "సత్వరమార్గం"

#: ../searx/plugins/answerer/ascii/answerer.py:42
msgid "Does some Ascii Stuff"
msgstr ""

#: ../searx/plugins/answerer/bitcoin/answerer.py:96
msgid "Bitcoin Index"
msgstr ""

#: ../searx/plugins/answerer/bitcoin/answerer.py:97
msgid "Get a the bitcoin index via currency or by default via USD, EUR and GBP"
msgstr ""

#: ../searx/plugins/answerer/bitcoin/templates/all.html:1
#: ../searx/plugins/answerer/bitcoin/templates/currency.html:1
msgid "The current Bitcoin index is"
msgstr ""

#: ../searx/plugins/answerer/cheatsheets/answerer.py:79
msgid "Cheatsheet Index"
msgstr ""

#: ../searx/plugins/answerer/cheatsheets/answerer.py:80
msgid "Cheatsheet module to load some awesome modules found around the web."
msgstr ""

#: ../searx/plugins/answerer/cheatsheets/modules/guitar/templates/all.html:1
msgid "Guitar Cheatsheet"
msgstr ""

#: ../searx/plugins/answerer/cheatsheets/modules/guitar/templates/all.html:24
msgid "Cheatsheet Images by {source}"
msgstr ""

#: ../searx/plugins/answerer/grade/answerer.py:82
msgid "{service} has a Privacy {grade} on ToS;DR"
msgstr ""

#: ../searx/plugins/answerer/grade/answerer.py:116
msgid "ToS;DR Grade"
msgstr ""

#: ../searx/plugins/answerer/grade/answerer.py:117
msgid "Get a grade from ToS;DR using the ToS;DR API"
msgstr ""

#: ../searx/plugins/answerer/hash/answerer.py:69
msgid "Hash Generator"
msgstr ""

#: ../searx/plugins/answerer/hash/answerer.py:70
msgid "Generate md4, md5, sha1, sha256 and sha512 hashes"
msgstr ""

#: ../searx/plugins/answerer/leetspeak/answerer.py:64
msgid "Leetspeak Answerer"
msgstr ""

#: ../searx/plugins/answerer/leetspeak/answerer.py:65
msgid "L 3 3 '][' 5 |D 3 /-\\ |< I 5 C 0 0 |_"
msgstr ""

#: ../searx/plugins/answerer/nato/answerer.py:66
msgid "Nato Converter"
msgstr ""

#: ../searx/plugins/answerer/nato/answerer.py:67
msgid "Converts words into the nato phonetic alphabet"
msgstr ""

#: ../searx/plugins/answerer/onion/answerer.py:40
msgid "Onion Notice"
msgstr ""

#: ../searx/plugins/answerer/onion/answerer.py:41
msgid "Give a notice if an onion address has been queried"
msgstr ""

#: ../searx/plugins/answerer/random/answerer.py:94
msgid "Random value generator"
msgstr ""

#: ../searx/plugins/answerer/random/answerer.py:95
msgid "Generate different random values"
msgstr ""

#: ../searx/templates/oscar/404.html:4
msgid "Page not found"
msgstr "పుట దొరకలేదు"

#: ../searx/templates/oscar/404.html:6
#, python-format
msgid "Go to %(search_page)s."
msgstr "%(search_page)sకు వెళ్ళు"

#: ../searx/templates/oscar/404.html:6
msgid "search page"
msgstr "శోధన పుట"

#: ../searx/templates/oscar/about.html:2 ../searx/templates/oscar/navbar.html:6
msgid "about"
msgstr "గురించి"

#: ../searx/templates/oscar/advanced.html:4
msgid "Advanced settings"
msgstr ""

#: ../searx/templates/oscar/base.html:77
#: ../searx/templates/oscar/messages/first_time.html:4
#: ../searx/templates/oscar/messages/save_settings_successfull.html:5
#: ../searx/templates/oscar/messages/unknow_error.html:5
msgid "Close"
msgstr "మూసివేయు"

#: ../searx/templates/oscar/base.html:79
#: ../searx/templates/oscar/messages/no_results.html:4
msgid "Error!"
msgstr "దోషం!"

#: ../searx/templates/oscar/base.html:107
msgid "Powered by {searx} and forked by {tosdr}"
msgstr ""

#: ../searx/templates/oscar/base.html:108
msgid "{version} on commit {hash}"
msgstr ""

#: ../searx/templates/oscar/base.html:109
msgid "a privacy-respecting, hackable metasearch engine"
msgstr ""

#: ../searx/templates/oscar/base.html:110
msgid "Source code"
msgstr ""

#: ../searx/templates/oscar/base.html:111
msgid "Issue tracker"
msgstr ""

#: ../searx/templates/oscar/base.html:113
msgid "Contact instance maintainer"
msgstr ""

#: ../searx/templates/oscar/base.html:115
msgid "Translate ToS;DR Search"
msgstr ""

#: ../searx/templates/oscar/languages.html:2
msgid "Language"
msgstr ""

#: ../searx/templates/oscar/languages.html:4
msgid "Default language"
msgstr "నిష్క్రియ భాష"

#: ../searx/templates/oscar/macros.html:23
msgid "magnet link"
msgstr ""

#: ../searx/templates/oscar/macros.html:24
msgid "torrent file"
msgstr ""

#: ../searx/templates/oscar/macros.html:36
#: ../searx/templates/oscar/macros.html:38
#: ../searx/templates/oscar/macros.html:72
#: ../searx/templates/oscar/macros.html:74
msgid "cached"
msgstr ""

#: ../searx/templates/oscar/macros.html:42
#: ../searx/templates/oscar/macros.html:58
#: ../searx/templates/oscar/macros.html:78
#: ../searx/templates/oscar/macros.html:92
msgid "proxied"
msgstr ""

#: ../searx/templates/oscar/macros.html:132
#: ../searx/templates/oscar/preferences.html:252
#: ../searx/templates/oscar/preferences.html:269
msgid "Allow"
msgstr "అనుమతించు"

#: ../searx/templates/oscar/macros.html:139
msgid "supported"
msgstr "ఆదరించబడిన"

#: ../searx/templates/oscar/macros.html:143
msgid "not supported"
msgstr "ఆదరణ లేని"

#: ../searx/templates/oscar/navbar.html:7
#: ../searx/templates/oscar/preferences.html:16
msgid "preferences"
msgstr "అభిరుచులు"

#: ../searx/templates/oscar/preferences.html:12
msgid "No HTTPS"
msgstr ""

#: ../searx/templates/oscar/preferences.html:21
msgid "Preferences"
msgstr "అభిరుచులు"

#: ../searx/templates/oscar/preferences.html:26
#: ../searx/templates/oscar/preferences.html:36
msgid "General"
msgstr "సాధారణ"

#: ../searx/templates/oscar/preferences.html:27
#: ../searx/templates/oscar/preferences.html:226
msgid "Engines"
msgstr "యంత్రాలు"

#: ../searx/templates/oscar/preferences.html:28
#: ../searx/templates/oscar/preferences.html:329
msgid "Plugins"
msgstr "ప్లగిన్లు"

#: ../searx/templates/oscar/preferences.html:30
#: ../searx/templates/oscar/preferences.html:359
msgid "Answerers"
msgstr "జవాబులు"

#: ../searx/templates/oscar/preferences.html:31
#: ../searx/templates/oscar/preferences.html:412
msgid "Cookies"
msgstr "కుకీలు"

#: ../searx/templates/oscar/preferences.html:49
#: ../searx/templates/oscar/preferences.html:52
msgid "Default categories"
msgstr "నిష్క్రియ వర్గాలు"

#: ../searx/templates/oscar/preferences.html:60
msgid "Search language"
msgstr "శోధన భాష"

#: ../searx/templates/oscar/preferences.html:61
msgid "What language do you prefer for search?"
msgstr ""

#: ../searx/templates/oscar/preferences.html:68
msgid "Interface language"
msgstr "వినిమయసీమ భాష"

#: ../searx/templates/oscar/preferences.html:69
msgid "Change the language of the layout"
msgstr "వినిమయసీమ యొక్క భాషను మార్చు"

#: ../searx/templates/oscar/preferences.html:81
msgid "Autocomplete"
msgstr ""

#: ../searx/templates/oscar/preferences.html:82
msgid "Find stuff as you type"
msgstr "టైపు చేస్తూ శోధించు"

#: ../searx/templates/oscar/preferences.html:96
msgid "Image proxy"
msgstr ""

#: ../searx/templates/oscar/preferences.html:97
msgid "Proxying image results through searx"
msgstr ""

#: ../searx/templates/oscar/preferences.html:102
msgid "Enabled"
msgstr ""

#: ../searx/templates/oscar/preferences.html:104
msgid "Disabled"
msgstr ""

#: ../searx/templates/oscar/preferences.html:110
msgid "Method"
msgstr "విధానం"

#: ../searx/templates/oscar/preferences.html:111
msgid ""
"Change how forms are submited, <a href=\"http://en.wikipedia.org/wiki/"
"Hypertext_Transfer_Protocol#Request_methods\" rel=\"external\">learn more "
"about request methods</a>"
msgstr ""

#: ../searx/templates/oscar/preferences.html:123
#: ../searx/templates/oscar/preferences.html:256
#: ../searx/templates/oscar/preferences.html:264
msgid "SafeSearch"
msgstr "సురక్షితశోధన"

#: ../searx/templates/oscar/preferences.html:124
msgid "Filter content"
msgstr "విషయాలను వడకట్టు"

#: ../searx/templates/oscar/preferences.html:129
msgid "Strict"
msgstr "కఠినమైన"

#: ../searx/templates/oscar/preferences.html:131
msgid "Moderate"
msgstr "మితమైన"

#: ../searx/templates/oscar/preferences.html:133
msgid "None"
msgstr "ఏమీ లేదు"

#: ../searx/templates/oscar/preferences.html:139
msgid "Themes"
msgstr ""

#: ../searx/templates/oscar/preferences.html:140
msgid "Change searx layout"
msgstr ""

#: ../searx/templates/oscar/preferences.html:152
#: ../searx/templates/oscar/preferences.html:160
msgid "Choose style for this theme"
msgstr ""

#: ../searx/templates/oscar/preferences.html:152
#: ../searx/templates/oscar/preferences.html:160
msgid "Style"
msgstr "శైలి"

#: ../searx/templates/oscar/preferences.html:164
msgid "Results on new tabs"
msgstr ""

#: ../searx/templates/oscar/preferences.html:165
msgid "Open result links on new browser tabs"
msgstr ""

#: ../searx/templates/oscar/preferences.html:170
#: ../searx/templates/oscar/preferences.html:183
msgid "On"
msgstr ""

#: ../searx/templates/oscar/preferences.html:172
#: ../searx/templates/oscar/preferences.html:185
msgid "Off"
msgstr ""

#: ../searx/templates/oscar/preferences.html:177
#, fuzzy
#| msgid "Invalid settings"
msgid "Show advanced settings"
msgstr "చెల్లని అమరికలు"

#: ../searx/templates/oscar/preferences.html:178
msgid "Show advanced settings panel in the home page by default"
msgstr ""

#: ../searx/templates/oscar/preferences.html:190
msgid "Open Access DOI resolver"
msgstr ""

#: ../searx/templates/oscar/preferences.html:191
msgid ""
"Redirect to open-access versions of publications when available (plugin "
"required)"
msgstr ""

#: ../searx/templates/oscar/preferences.html:205
msgid "Engine tokens"
msgstr ""

#: ../searx/templates/oscar/preferences.html:206
msgid "Access tokens for private engines"
msgstr ""

#: ../searx/templates/oscar/preferences.html:235
msgid "Allow all"
msgstr ""

#: ../searx/templates/oscar/preferences.html:237
msgid "Disable all"
msgstr ""

#: ../searx/templates/oscar/preferences.html:253
#: ../searx/templates/oscar/preferences.html:268
msgid "Engine name"
msgstr "యంత్రం పేరు"

#: ../searx/templates/oscar/preferences.html:254
#: ../searx/templates/oscar/preferences.html:267
msgid "Shortcut"
msgstr "సత్వరమార్గం"

#: ../searx/templates/oscar/preferences.html:255
#: ../searx/templates/oscar/preferences.html:266
msgid "Selected language"
msgstr "ఎంచుకున్న భాష"

#: ../searx/templates/oscar/preferences.html:257
#: ../searx/templates/oscar/preferences.html:263
#: ../searx/templates/oscar/time-range.html:2
msgid "Time range"
msgstr "కాల శ్రేణి"

#: ../searx/templates/oscar/preferences.html:258
#: ../searx/templates/oscar/preferences.html:262
msgid "Avg. time"
msgstr "సగటు సమయం"

#: ../searx/templates/oscar/preferences.html:259
#: ../searx/templates/oscar/preferences.html:261
msgid "Max time"
msgstr "గరిష్ఠ సమయం"

#: ../searx/templates/oscar/preferences.html:362
msgid "This is the list of searx's instant answering modules."
msgstr ""

#: ../searx/templates/oscar/preferences.html:366
msgid "Name"
msgstr "పేరు"

#: ../searx/templates/oscar/preferences.html:367
#: ../searx/templates/oscar/results.html:191
#: ../searx/templates/oscar/results.html:196
#: ../searx/templates/oscar/results.html:204
#: ../searx/templates/oscar/results.html:209
msgid "Developer"
msgstr ""

#: ../searx/templates/oscar/preferences.html:368
msgid "Description"
msgstr "వర్ణన"

#: ../searx/templates/oscar/preferences.html:369
msgid "Examples"
msgstr "ఉదాహరణలు"

#: ../searx/templates/oscar/preferences.html:415
msgid ""
"This is the list of cookies and their values searx is storing on your "
"computer."
msgstr ""

#: ../searx/templates/oscar/preferences.html:416
msgid "With that list, you can assess searx transparency."
msgstr ""

#: ../searx/templates/oscar/preferences.html:421
msgid "Cookie name"
msgstr "కుకీ పేరు"

#: ../searx/templates/oscar/preferences.html:422
msgid "Value"
msgstr "విలువ"

#: ../searx/templates/oscar/preferences.html:439
msgid ""
"These settings are stored in your cookies, this allows us not to store this "
"data about you."
msgstr ""

#: ../searx/templates/oscar/preferences.html:440
msgid ""
"These cookies serve your sole convenience, we don't use these cookies to "
"track you."
msgstr ""

#: ../searx/templates/oscar/preferences.html:444
msgid "Search URL of the currently saved preferences"
msgstr ""

#: ../searx/templates/oscar/preferences.html:445
msgid ""
"Note: specifying custom settings in the search URL can reduce privacy by "
"leaking data to the clicked result sites."
msgstr ""

#: ../searx/templates/oscar/preferences.html:452
msgid "save"
msgstr "దాచు"

#: ../searx/templates/oscar/preferences.html:454
msgid "back"
msgstr "వెనక్కి"

#: ../searx/templates/oscar/preferences.html:457
msgid "Reset defaults"
msgstr "నిష్క్రియాలకు అమర్చు"

#: ../searx/templates/oscar/results.html:39
msgid "Engines cannot retrieve results"
msgstr "యంత్రాలు ఫలితాలను రాబట్టలేకపోతున్నాయి"

#: ../searx/templates/oscar/results.html:55
msgid "Suggestions"
msgstr "సూచనలు"

#: ../searx/templates/oscar/results.html:77
msgid "Links"
msgstr "లంకెలు"

#: ../searx/templates/oscar/results.html:82
msgid "Search URL"
msgstr "శోధన URL"

#: ../searx/templates/oscar/results.html:88
msgid "Download results"
msgstr "ఫలితాలను దింపుకోండి"

#: ../searx/templates/oscar/results.html:100
msgid "RSS subscription"
msgstr ""

#: ../searx/templates/oscar/results.html:107
msgid "Search results"
msgstr ""

#: ../searx/templates/oscar/results.html:112
msgid "Try searching for:"
msgstr "దీనికొరకు శోధించండి:"

#: ../searx/templates/oscar/results.html:159
msgid "No Description provided by the developer"
msgstr ""

#: ../searx/templates/oscar/results.html:166
msgid "Issue Tracker"
msgstr ""

#: ../searx/templates/oscar/results.html:175
msgid ""
"Source\n"
"                                                        Code"
msgstr ""

#: ../searx/templates/oscar/results.html:183
msgid "Website"
msgstr ""

#: ../searx/templates/oscar/results.html:253
#: ../searx/templates/oscar/results.html:283
#: ../searx/templates/oscar/results.html:296
msgid "Missing Text"
msgstr ""

#: ../searx/templates/oscar/results.html:333
#: ../searx/templates/oscar/results.html:367
msgid "next page"
msgstr "తర్వాతి పుట"

#: ../searx/templates/oscar/results.html:343
#: ../searx/templates/oscar/results.html:357
msgid "previous page"
msgstr "పూర్వపు పుట"

#: ../searx/templates/oscar/search.html:6
#: ../searx/templates/oscar/search_full.html:9
msgid "Search for..."
msgstr "శోధించు..."

#: ../searx/templates/oscar/search.html:8
#: ../searx/templates/oscar/search_full.html:11
msgid "Start search"
msgstr "శోధన ప్రారంభించు"

#: ../searx/templates/oscar/search.html:9
#: ../searx/templates/oscar/search_full.html:12
msgid "Clear search"
msgstr ""

#: ../searx/templates/oscar/search_full.html:12
msgid "Clear"
msgstr ""

#: ../searx/templates/oscar/stats.html:2
msgid "stats"
msgstr "స్థితి వివరణ లెక్కలు"

#: ../searx/templates/oscar/stats.html:5
msgid "Engine stats"
msgstr ""

#: ../searx/templates/oscar/time-range.html:5
msgid "Anytime"
msgstr "ఎప్పుడైనా"

#: ../searx/templates/oscar/time-range.html:8
msgid "Last day"
msgstr "క్రిందటి రోజు"

#: ../searx/templates/oscar/time-range.html:11
msgid "Last week"
msgstr "క్రిందటి వారం"

#: ../searx/templates/oscar/time-range.html:14
msgid "Last month"
msgstr "క్రిందటి నెల"

#: ../searx/templates/oscar/time-range.html:17
msgid "Last year"
msgstr "క్రిందటి సంవత్సరం"

#: ../searx/templates/oscar/messages/first_time.html:6
#: ../searx/templates/oscar/messages/no_data_available.html:3
msgid "Heads up!"
msgstr "జాగ్రత్త!"

#: ../searx/templates/oscar/messages/first_time.html:7
msgid "It look like you are using searx first time."
msgstr ""

#: ../searx/templates/oscar/messages/no_cookies.html:3
msgid "Information!"
msgstr "సమాచారం!"

#: ../searx/templates/oscar/messages/no_cookies.html:4
msgid "currently, there are no cookies defined."
msgstr ""

#: ../searx/templates/oscar/messages/no_data_available.html:4
msgid "There is currently no data available. "
msgstr ""

#: ../searx/templates/oscar/messages/no_results.html:4
msgid "Engines cannot retrieve results."
msgstr "యంత్రాలు ఫలితాలను రాబట్టలేకపోయాయి."

#: ../searx/templates/oscar/messages/no_results.html:10
msgid "Please, try again later or find another searx instance."
msgstr ""

#: ../searx/templates/oscar/messages/no_results.html:10
msgid "Public instances"
msgstr ""

#: ../searx/templates/oscar/messages/no_results.html:14
msgid "Sorry!"
msgstr "క్షమించండి!"

#: ../searx/templates/oscar/messages/no_results.html:15
msgid ""
"we didn't find any results. Please use another query or search in more "
"categories."
msgstr ""

#: ../searx/templates/oscar/messages/save_settings_successfull.html:7
msgid "Well done!"
msgstr "భళా!"

#: ../searx/templates/oscar/messages/save_settings_successfull.html:8
msgid "Settings saved successfully."
msgstr "ఆమరికలు విజయవంతంగా పొందుపరచబడ్డాయి."

#: ../searx/templates/oscar/messages/unknow_error.html:7
msgid "Oh snap!"
msgstr "అయ్యో!"

#: ../searx/templates/oscar/messages/unknow_error.html:8
msgid "Something went wrong."
msgstr "ఏదో తప్పు జరిగింది."

#: ../searx/templates/oscar/result_templates/default.html:7
#: ../searx/templates/oscar/result_templates/files.html:7
#: ../searx/templates/oscar/result_templates/files.html:10
msgid "show media"
msgstr ""

#: ../searx/templates/oscar/result_templates/default.html:7
#: ../searx/templates/oscar/result_templates/files.html:7
msgid "hide media"
msgstr ""

#: ../searx/templates/oscar/result_templates/files.html:33
#: ../searx/templates/oscar/result_templates/videos.html:19
msgid "Author"
msgstr ""

#: ../searx/templates/oscar/result_templates/files.html:35
#, fuzzy
#| msgid "Engine name"
msgid "Filename"
msgstr "యంత్రం పేరు"

#: ../searx/templates/oscar/result_templates/files.html:37
#: ../searx/templates/oscar/result_templates/torrent.html:7
msgid "Filesize"
msgstr ""

#: ../searx/templates/oscar/result_templates/files.html:38
#: ../searx/templates/oscar/result_templates/torrent.html:9
msgid "Bytes"
msgstr ""

#: ../searx/templates/oscar/result_templates/files.html:39
#: ../searx/templates/oscar/result_templates/torrent.html:10
msgid "kiB"
msgstr ""

#: ../searx/templates/oscar/result_templates/files.html:40
#: ../searx/templates/oscar/result_templates/torrent.html:11
msgid "MiB"
msgstr ""

#: ../searx/templates/oscar/result_templates/files.html:41
#: ../searx/templates/oscar/result_templates/torrent.html:12
msgid "GiB"
msgstr ""

#: ../searx/templates/oscar/result_templates/files.html:42
#: ../searx/templates/oscar/result_templates/torrent.html:13
msgid "TiB"
msgstr ""

#: ../searx/templates/oscar/result_templates/files.html:46
msgid "Date"
msgstr ""

#: ../searx/templates/oscar/result_templates/files.html:48
msgid "Type"
msgstr ""

#: ../searx/templates/oscar/result_templates/images.html:27
msgid "Get image"
msgstr ""

#: ../searx/templates/oscar/result_templates/images.html:30
msgid "View source"
msgstr ""

#: ../searx/templates/oscar/result_templates/map.html:7
msgid "show map"
msgstr ""

#: ../searx/templates/oscar/result_templates/map.html:7
msgid "hide map"
msgstr ""

#: ../searx/templates/oscar/result_templates/map.html:11
msgid "show details"
msgstr ""

#: ../searx/templates/oscar/result_templates/map.html:11
msgid "hide details"
msgstr ""

#: ../searx/templates/oscar/result_templates/torrent.html:6
msgid "Seeder"
msgstr ""

#: ../searx/templates/oscar/result_templates/torrent.html:6
msgid "Leecher"
msgstr ""

#: ../searx/templates/oscar/result_templates/torrent.html:15
msgid "Number of Files"
msgstr "ఫైళ్ళ సంఖ్య"

#: ../searx/templates/oscar/result_templates/videos.html:7
msgid "show video"
msgstr "వీడియో చూపించు"

#: ../searx/templates/oscar/result_templates/videos.html:7
msgid "hide video"
msgstr "వీడియోను దాచిపెట్టు"

#: ../searx/templates/oscar/result_templates/videos.html:20
msgid "Length"
msgstr ""

#~ msgid "Color"
#~ msgstr "రంగు"

#~ msgid "Blue (default)"
#~ msgstr "నీలం (నిష్క్రియం)"

#~ msgid "Violet"
#~ msgstr "ఊదారంగు"

#~ msgid "Green"
#~ msgstr "ఆకుపచ్చ"

#~ msgid "Cyan"
#~ msgstr " ముదురు నీలం"

#~ msgid "Orange"
#~ msgstr "నారింజ"

#~ msgid "Red"
#~ msgstr "ఎరుపు"

#~ msgid "Currently used search engines"
#~ msgstr "ప్రస్తుతం ఉపయోగించబడుతున్న శోధన యంత్రాలు"

#~ msgid "Category"
#~ msgstr "వర్గము"

#~ msgid "Block"
#~ msgstr "అడ్డగించు"

#~ msgid "Answers"
#~ msgstr "జవాబులు"

#~ msgid "Privacy"
#~ msgstr "ఆంతరంగికత"
